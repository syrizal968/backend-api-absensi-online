<?php

namespace Database\Seeders;

use App\Models\Absensi;
use App\Models\Kelas;
use App\Models\Siswa;
use App\Models\User;
use App\Models\Mapel;
// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // \App\Models\User::factory(1)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'rizal',
        //     'email' => 'rizal@gmail.com',
        // ]);

        User::create([
            'name' => 'rizal',
            'email' => 'rizal@gmail.com',
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', //password
        ]);

        Kelas::create([
            'name' => 'A20',
        ]);
        Kelas::create([
            'name' => 'B20',
        ]);
        Kelas::create([
            'name' => 'C20',
        ]);

        Mapel::create( [
            'name' => 'Web',
            'kelas_id' => '1',
            'user_id' => '1',
        ]);

        Mapel::create([
            'name' => 'mobile',
            'kelas_id' => '1',
            'user_id' => '1',
        ]);
        Mapel::create([
            'name' => 'Web',
            'kelas_id' => '2',
            'user_id' => '1',
        ]);

        Mapel::create([
            'name' => 'mobile',
            'kelas_id' => '2',
            'user_id' => '1',
        ]);
        Mapel::create([
            'name' => 'Web',
            'kelas_id' => '3',
            'user_id' => '1',
        ]);

        Mapel::create([
            'name' => 'mobile',
            'kelas_id' => '3',
            'user_id' => '1',
        ]);

        Absensi::create([
            'keterangan' => 'hadir',
        ]);
        Absensi::create([
            'keterangan' => 'izin',
        ]);
        Absensi::create([
            'keterangan' => 'sakit',
        ]);
        Absensi::create([
            'keterangan' => 'tampa keteraangan',
        ]);

        Siswa::create([
            'name' => 'ando',
            'jenis_kelamin' => 'L',
            'kelas_id' => '1',
        ]);
        Siswa::create([
            'name' => 'anti',
            'jenis_kelamin' => 'P',
            'kelas_id' => '1',
        ]);
        Siswa::create([
            'name' => 'yandi',
            'jenis_kelamin' => 'L',
            'kelas_id' => '2',
        ]);
        Siswa::create([
            'name' => 'dinda',
            'jenis_kelamin' => 'P',
            'kelas_id' => '2',
        ]);
        Siswa::create([
            'name' => 'yanto',
            'jenis_kelamin' => 'L',
            'kelas_id' => '3',
        ]);
        Siswa::create([
            'name' => 'andin',
            'jenis_kelamin' => 'P',
            'kelas_id' => '3',
        ]);
    }
}
