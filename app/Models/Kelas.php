<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;


class Kelas extends Model
{
    use HasFactory;

    protected $fillable = ['name'];

    /**
     * Get all of the Siswa for the Kelas
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function Siswa(): HasMany
    {
        return $this->hasMany(Siswa::class);
    }

    /**
     * Get all of the Mapel for the Kelas
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function Mapel(): HasMany
    {
        return $this->hasMany(Mapel::class);
    }

    public function Presensi(): HasMany
    {
        return $this->hasMany(Presensi::class);
    }

    /**
     * Get the Absensi that owns the Kelas
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
}
