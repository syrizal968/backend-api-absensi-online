<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Absensi extends Model
{
    use HasFactory;

    protected $fillable = ['keterangan'];


    /**
     * Get all of the Presensi for the Absensi
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function Presensi(): HasMany
    {
        return $this->hasMany(Presensi::class);
    }
}
