<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
class Presensi extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function Kelas(): BelongsTo
    {
        return $this->belongsTo(Kelas::class);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function Mapel(): BelongsTo
    {
        return $this->belongsTo(Mapel::class);
    }

    public function Absensisensi(): BelongsTo
    {
        return $this->belongsTo(Absensisensi::class);
    }

    public function Siswa(): BelongsTo
    {
        return $this->belongsTo(Siswa::class);
    }
}
