<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = ['photo', 'name', 'email', 'password'];


    protected function image(): Attribute
    {
        return Attribute::make(
            get: fn ($image) => asset('/storage/photouser/' . $image),
        );
    }

    /**
     * Get all of the Mapel for the User
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function Mapel(): HasMany
    {
        return $this->hasMany(Mapel::class);
    }

    /**
     * Get all of the Presensi for the User
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function Presensi(): HasMany
    {
        return $this->hasMany(Presensi::class);
    }

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
