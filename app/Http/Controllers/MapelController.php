<?php

namespace App\Http\Controllers;

use App\Models\Mapel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\MapelResource;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\MapeldetailResource;

class MapelController extends Controller
{
    public function index()
    {
        $mapels = Mapel::get();
        // return response()->json(['data' => $kelas]);
        return MapelResource::collection($mapels);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [ 
            'name' => 'required',
            'kelas_id' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $request['user_id'] = Auth::user()->id;
        $mapel = Mapel::create($request->all());
        return new MapeldetailResource($mapel->loadMissing('User:id,name'));
    }

    public function show($id)
    {
        $mapel = Mapel::with(['Kelas:id,name', 'User:id,name',])->findOrFail($id);
        return new MapeldetailResource($mapel);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [ 
            'name' => 'required',
            'kelas_id' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $mapel = Mapel::findOrFail($id);
        $mapel->update($request->all());

        return new MapeldetailResource($mapel->loadMissing('User:id,name'));
    }

    public function destroy($id)
    {
        $mapel = Mapel::findOrFail($id);
        $mapel->delete();

        return new MapeldetailResource($mapel->loadMissing('User:id,name'));
    }
}
