<?php

namespace App\Http\Controllers;

use App\Models\Kelas;
use App\Models\Mapel;
use App\Models\Siswa;
use App\Models\Absensi;
use App\Models\Presensi;
use Illuminate\Http\Request;
use App\Http\Resources\PresensidetailResource;
use App\Http\Resources\PresensisiswadetailResource;

class PresensiController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $kelas = Kelas::get();
        return PresensidetailResource::collection($kelas->loadMissing(['Mapel:id,name,kelas_id']));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $presensi =Presensi::create($request->all());
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $kelas = Kelas::with(['Mapel:name,kelas_id', 'Siswa:id,name,kelas_id',])->findOrFail($id);
        // $absensi = Absensi::where('id', '!=', 2)get()
        return new PresensisiswadetailResource($kelas);


        
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
