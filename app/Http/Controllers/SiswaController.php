<?php

namespace App\Http\Controllers;

use App\Models\Siswa;
use Illuminate\Http\Request;
use App\Http\Resources\SiswaResource;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\SiswadetailResource;

class SiswaController extends Controller
{
    public function index()
    {
        $siswa = Siswa::get();
        // return response()->json(['data' => $siswa]);
        return SiswaResource::collection($siswa);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [ 
            'name' => 'required',
            'Jenis_kelamin' => 'required',
            'kelas_id' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $siswa = Siswa::create($request->all());
        return new SiswadetailResource($siswa->loadMissing('Kelas:id,name'));
    }

    public function show($id)
    {
        $siswa = Siswa::with(['Kelas:id,name'])->findOrFail($id);
        return new SiswadetailResource($siswa);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [ 
            'name' => 'required',
            'Jenis_kelamin' => 'required',
            'kelas_id' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $siswa = Siswa::findOrFail($id);
        $siswa->update($request->all());

        return new SiswadetailResource($siswa->loadMissing('Kelas:id,name'));
    }

    public function destroy($id)
    {
        $siswa = Siswa::findOrFail($id);
        $siswa->delete();

        return new SiswadetailResource($siswa->loadMissing('Kelas:id,name'));
    }
}
