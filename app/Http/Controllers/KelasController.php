<?php

namespace App\Http\Controllers;

use App\Models\Kelas;
use Illuminate\Http\Request;
use App\Http\Resources\KelasResource;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\KelasdetailResource;

class KelasController extends Controller
{
    public function index()
    {
        $kelas = Kelas::get();
        // return response()->json(['data' => $kelas]);
        // return new KelasResource([true, 'data kelas' ,$kelas]);
        return KelasResource::collection($kelas);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [ 
            'name' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
        
        $kelas = Kelas::create($request->all());
        return response()->json(['message' => 'Data Berhasil di Tambah']);
    }

    public function show($id)
    {
        $kelas = Kelas::with(['Mapel:id,name,kelas_id', 'Siswa:id,name,kelas_id'])->findOrFail($id);
        return new KelasdetailResource($kelas);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [ 
            'name' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $kelas = Kelas::findOrFail($id);
        $kelas->update($request->all());

        return response()->json(['message' => 'Data Berhasil di Update']);

    }

    public function destroy($id)
    {
        $kelas = Kelas::findOrFail($id);
        $kelas->delete();

        return response()->json(['message' => 'Data Berhasil di Hapus']);
        
    }
}
