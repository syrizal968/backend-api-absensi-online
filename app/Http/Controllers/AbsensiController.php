<?php

namespace App\Http\Controllers;
use App\Models\Absensi;
use App\Models\Presensi;
use Illuminate\Http\Request;
use App\Http\Resources\AbsensiResource;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\AbsensidetailResource;

class AbsensiController extends Controller
{
    public function index()
    {
        $absensi = Absensi::get();
        // return response()->json(['data' => $kelas]);
        return AbsensiResource::collection($absensi);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'keterangan' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $absensi = Absensi::create($request->all());
        return new AbsensiResource($absensi);
    }

    public function show($id)
    {
        $absensi = Absensi::findOrFail($id);
        return new AbsensidetailResource($absensi);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'keterangan' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $absensi = Absensi::findOrFail($id);
        $absensi->update($request->all());

        return new AbsensidetailResource($absensi);
    }

    public function destroy($id)
    {
        $absensi = Absensi::findOrFail($id);
        $absensi->delete();

        return new AbsensidetailResource($absensi);
    }
}
