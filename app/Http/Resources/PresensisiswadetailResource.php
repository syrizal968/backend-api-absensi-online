<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class PresensisiswadetailResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'Mapel' => $this->whenLoaded('Mapel'),
            // 'User' => $this->whenLoaded('User'),
            'Kelas' => $this->whenLoaded('Kelas'),
            'Presensi' => $this->whenLoaded('Presensi'),
            'Absensi' => $this->whenLoaded('Absensi'),

        ];
    }
}
