<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class MapeldetailResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'kelas_id' => $this->kelas_id,
            'user_id' => $this->user_id,
            'Kelas' => $this->whenLoaded('Kelas'),
            'User' => $this->whenLoaded('User'),
        ];
    }
}
