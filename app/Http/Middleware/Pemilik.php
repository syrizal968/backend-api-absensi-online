<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Mapel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class Pemilik
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $currentUser = Auth::user();
        $mapel = Mapel::findOrFail($request->id);
        if ($mapel->user_id != $currentUser->id) {
            return response()->json(['massage' => 'data not found'], 404);
        }
        return $next($request);
    }
}
